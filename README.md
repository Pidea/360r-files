# 360R-files

This repository contains the .deb files for the latest release of the
360Radar MLAT and ADSB clients for different versions of Python 3.

## How to get the files

The files are sorted by platform, client name and Python3.x version thus:

<platform> / <client_name> / <Python3_version> / <client_name>_vession.deb

A link will be provided from the installation page directly to each file 
based on the platform, client and Python3 version.

The installation instructions can be found here: <add_link>



